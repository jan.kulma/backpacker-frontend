const check = () => {
  if (!("serviceWorker" in navigator)) {
    throw new Error("No Service Worker support!");
  }
  if (!("PushManager" in window)) {
    throw new Error("No Push API Support!");
  }
};

const registerServiceWorker = async () => {
  const swRegistration = await navigator.serviceWorker.register(`${process.env.BASE_URL}lol.js`);
  return swRegistration;
};

const main = async () => {
  check()

  registerServiceWorker()
  	.then(function (reg) {

  		const applicationServerKey = urlB64ToUint8Array(
	      "BLbMXrE_UDUYnPwsdUVoxPYklVGDhlhWvJA7C9PZaWLwIf8rbagY1u8m_l4Z9xmQZp5BBWqd3I3pU-jiza4Ah84"
	    );
    	const options = { applicationServerKey, userVisibleOnly: true };

  	  return reg.pushManager.subscribe(options);
  	})
  	.then(function(sub) {


      axios.post('/user')



  		console.log(sub);

  		let base = 'https://0ac46ef7.ngrok.io/';


  		  // const SERVER_URL = 'http://localhost:1337/notification/save'
  		  // fetch(SERVER_URL, {
  		  //   method: 'post',
  		  //   headers: {
  		  //     'Content-Type': 'application/json',
  		  //   },
  		  //   body: JSON.stringify(sub),
  		  // })
  		  // .then(function (res) { return res.json()})
  		  // .then(function (json) { console.log(json) })

  	})
  // const permission = await requestNotificationPermission();
};

// urlB64ToUint8Array is a magic function that will encode the base64 public key
// to Array buffer which is needed by the subscription option
const urlB64ToUint8Array = base64String => {
  const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, "+")
    .replace(/_/g, "/");
  const rawData = atob(base64);
  const outputArray = new Uint8Array(rawData.length);
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
};

self.addEventListener("install", async () => {
	console.log(222222222)
  // This will be called only once when the service worker is installed for first time.
  try {
    const applicationServerKey = urlB64ToUint8Array(
      "BLbMXrE_UDUYnPwsdUVoxPYklVGDhlhWvJA7C9PZaWLwIf8rbagY1u8m_l4Z9xmQZp5BBWqd3I3pU-jiza4Ah84"
    );
    const options = { applicationServerKey, userVisibleOnly: true };
    const subscription = await self.registration.pushManager.subscribe(options);
    console.log(subscription);
  } catch (err) {
    console.log("Error", err);
  }
});

self.addEventListener("push", function(event) {
  if (event.data) {
    console.log("Push event!! ", event.data.text());
  } else {
    console.log("Push event but no data");
  }
});

// main()