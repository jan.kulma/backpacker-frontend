import './setUpSocket'
window.axios = require('axios')
axios.defaults.baseURL = 'http://localhost:1337'
require('./registerServiceWorker')
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import './assets/styles.css'

Vue.use(Buefy)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
