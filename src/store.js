import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
Vue.use(Vuex)



// State

const getBaseState = () => {
  return {
    jwt: '',
    user: {
      id: '',
      email: '',
      city: '',
      country: '',
    },
    hostingRequests: [],
  }
}

// Mutations

let mutations = {

  user (state, user) {
    state.user = {
      id: user.id,
      email: user.email,
      city: user.city,
      country: user.country,
    }

  },

  jwt (state, jwt) {
    state.jwt = jwt
    sessionStorage.setItem('jwt', jwt)
    io.sails.headers = {jwt: jwt}
    axios.defaults.headers.common['jwt'] = jwt
  },

  hostingRequests (state, hostingRequests) {
    hostingRequests.forEach(hostingRequest => {
      state.hostingRequests.push(hostingRequest)
    })
  },

  resetState (state) {
    Object.assign(state, getBaseState())
  },
}

// Actions

let actions = {

  getHostingRequests (context) {
    const city = context.state.user.city,
      now  = +new Date

    axios.get(`/hostingRequest?where={"city":"${city}", "dateFrom":{">":${now}}}`)
      .then(res => context.commit('hostingRequests', res.data))
      .catch(err => console.log(err))
  },

  async logIn (context, data) {
    let response
    try {
      response = await axios.post('user/login', data)
    } catch(err) {
      console.log(err)
      alert('Error')
      return
    }

    console.log(response)

    context.commit('user', response.data.user)
    context.commit('jwt', response.data.jwt)

    await context.dispatch('connectSocket')

    router.push({name: 'requests'})
  },

  async connectSocket (context) {
    io.socket = io.sails.connect()
    await context.dispatch('subscribeToUserRoom')
  },

  logOut (context, jwt) {
    console.log('WAAATT')
    sessionStorage.removeItem('jwt')
    io.socket.removeAllListeners()
    context.commit('resetState')
  },

  subscribeToUserRoom (context) {
    io.socket.get('/socket/join-room', (body, JWR) => {
      if (JWR.error) { return console.log(JWR.error)}
    })
    context.dispatch('registerSocketListeners')
  },

  registerSocketListeners (context) {
    io.socket.on('newHostingRequest', function (body) {
      context.commit('hostingRequests', [body])
    })
  },
  
  initialize (context) {
    if (sessionStorage.getItem('jwt')) {
      console.log('JWT found in sessionStorage')

      context.commit('jwt', sessionStorage.getItem('jwt'))

      context.dispatch('logIn', {})
    }
  }
}

let vuex = new Vuex.Store({state: getBaseState(), mutations, actions})

export default vuex