import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Home from './views/Home.vue'
import Requests from './views/Requests.vue'
import Chats from './views/Chats.vue'
import Profile from './views/Profile.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/requests',
      name: 'requests',
      component: Requests
    },
    {
      path: '/chats',
      name: 'chats',
      component: Chats
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    }
  ],
})

// return false if middleware should break the execution of middleware chain
let middleware = {

  preventNotLoggedIn: function (to, from, next) {

    const isUserLoggedIn = store.state.jwt
    console.log('is user logged in: ' + isUserLoggedIn)
    const logInRoute = '/'

    if (!isUserLoggedIn && to.path !== logInRoute) {
      next(logInRoute)
      return false
    }

    // dont go to next middleware if user is not logged in and tried to access log in route
    if (!isUserLoggedIn  && to.path === logInRoute) {
      next()
      return false
    }

    return true
  },

  redirectLoggedInUserFromLogInRoute: function (to, from, next) {

    const logInRoute = '/'
    const redirectRoute = '/requests'

    if (to.path === logInRoute) {
       next(redirectRoute)
       return false
    }

    return true
  },

  closingMiddleware: function (to, from, next) {
    next()
  }
}


router.beforeEach((to, from, next) => {
  for (let m in middleware) {
    console.log(m)
    if (!middleware[m](to, from, next, router)) break
  }
})

export default router