var socketIOClient = require('socket.io-client')
var sailsIOClient = require('sails.io.js')

// Instantiate the socket client (`io`)
// (for now, you must explicitly pass in the socket.io client when using this library from Node.js)
window.io = sailsIOClient(socketIOClient)
var url = 'http://localhost:1337'
io.sails.url = url
// io.sails.autoConnect = false